<?php

namespace Drupal\steam_login\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Steam Open Id Provider.
 */
class OpenIdProvider {

  public const STEAM_OPENID_URL = 'https://steamcommunity.com/openid/login';

  public const STEAM_OPENID_COMMUNITYID_REGEX = '/^https:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/';

  public const OPENID_NS = 'http://specs.openid.net/auth/2.0';

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * The current request.
   *
   * @var ?\Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Class constructor.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RequestStack $requestStack,
  ) {
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * Build OpenId URL.
   *
   * @param string $redirectUrl
   *   The URL we should redirect to.
   *
   * @return \Drupal\Core\Url
   *   The Open ID URL object.
   */
  public function buildOpenIdUrl(string $redirectUrl): Url {
    $params = [
      'openid.ns' => self::OPENID_NS,
      'openid.mode' => 'checkid_setup',
      'openid.return_to' => $redirectUrl,
      'openid.identity' => self::OPENID_NS . '/identifier_select',
      'openid.claimed_id' => self::OPENID_NS . '/identifier_select',
    ];

    return Url::fromUri(self::STEAM_OPENID_URL, ['query' => $params]);
  }

  /**
   * Retrieve the open id mode from current request.
   *
   * @return string
   *   The open id mode or an empty string.
   */
  public function getOpenIdMode(): string {
    return $this->currentRequest->query->getString('openid_mode');
  }

  /**
   * Retrieve the open id identity from current request.
   *
   * @return string
   *   The open id identity or an empty string.
   */
  public function getOpenIdIdentity(): string {
    return $this->currentRequest->query->getString('openid_identity');
  }

  /**
   * Get Steam community Id from an Openid identity.
   *
   * @return string
   *   The user Steam 64 Id or an empty string.
   */
  public function getSteamCommunityId(): string {
    $identity = $this->getOpenIdIdentity();
    if (empty($identity)) {
      return $identity;
    }
    preg_match(self::STEAM_OPENID_COMMUNITYID_REGEX, $identity, $matches);

    return $matches[1] ?? '';
  }

  /**
   * Creates a drupal user from steam community id.
   *
   * @param string $steamCommunityId
   *   The user steam community id.
   * @param string $steamNickname
   *   The user steam community nickname.
   *
   * @return ?\Drupal\user\UserInterface
   *   Either a user object or NULL.
   *
   * @todo Allow other modules to alter user before save.
   */
  public function steamOauthCreateUser(string $steamCommunityId, string $steamNickname) {
    if (empty($steamCommunityId)) {
      return NULL;
    }

    $userInfos = [
      'name' => "steam-$steamCommunityId",
      'pass' => '',
      'status' => 1,
      'field_steam64id' => $steamCommunityId,
      'field_steam_username' => urlencode($steamNickname),
    ];

    $user = $this->userStorage->create($userInfos);
    $user->save();
    return $user;
  }

  /**
   * Checks if a user already exists with the given steam community id.
   *
   * @param string $steamCommunityId
   *   The user steam community id.
   *
   * @return ?\Drupal\user\UserInterface
   *   Either a user object or NULL.
   */
  public function isSteamIdAlreadyUsed(string $steamCommunityId) {
    $users = $this->userStorage->loadByProperties(['field_steam64id' => $steamCommunityId]);
    if (!empty($users)) {
      return reset($users);
    }

    return NULL;
  }

  /**
   * Connect the user from steam.
   *
   * @param \Drupal\user\UserInterface $user
   *   Log the current user as the user we give.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The redirect response.
   */
  public function userConnectFromSteam(UserInterface $user): TrustedRedirectResponse {
    user_login_finalize($user);
    $userUrl = Url::fromRoute('entity.user.canonical', ['user' => $user->id()]);
    // $this->currentRequest->query->set('destination', $userUrl->toString());
    $response = new TrustedRedirectResponse($userUrl->toString());
    $response->addCacheableDependency($user);

    return $response;
  }

}
