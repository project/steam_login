<?php

namespace Drupal\steam_login\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\steam_api\ISteamUserInterface;
use Drupal\steam_login\Service\OpenIdProvider;
use Drupal\user\UserInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle the user connection via steam.
 */
class OpenIdAuth implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Steam OpenIdAuth constructor.
   */
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected ISteamUserInterface $iSteamUser,
    protected Client $httpClient,
    protected OpenIdProvider $openIdProvider,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('current_user'),
      $container->get('steam_api.user'),
      $container->get('http_client'),
      $container->get('steam_login.openid_provider'),
    );
  }

  /**
   * Build the controller response.
   *
   * @return array|TrustedRedirectResponse
   *   A renderable array or a redirect response.
   */
  public function __invoke(): array|TrustedRedirectResponse {
    $build = [];

    if ($this->currentUser->isAnonymous()) {

      switch ($this->openIdProvider->getOpenIdMode()) {
        case 'cancel':
          $build['canceled'] = [
            '#markup' => $this->t("You canceled the action."),
          ];
          break;

        case 'id_res':
          $steamCommunityId = $this->openIdProvider->getSteamCommunityId();
          $playerSummaries = current($this->iSteamUser
            ->getPlayerSummaries($steamCommunityId));

          $user = $this->openIdProvider->isSteamIdAlreadyUsed($steamCommunityId);
          if (is_null($user)) {
            $user = $this->openIdProvider->steamOauthCreateUser($steamCommunityId, $playerSummaries['personaname'] ?? '');
          }
          return $this->openIdProvider->userConnectFromSteam($user);

        default:
          $callbackUrl = Url::fromRoute('steam_login.openid', [], ['absolute' => TRUE])
            ->toString(TRUE);
          $url = $this->openIdProvider->buildOpenIdUrl($callbackUrl->getGeneratedUrl());
          $response = new TrustedRedirectResponse($url->toString());
          $response->addCacheableDependency($callbackUrl);

          return $response;
      }
    }

    $build['already_logged'] = [
      '#markup' => $this->t("You are already connected."),
    ];

    return $build;
  }

  /**
   * User route title callback.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   The user account.
   *
   * @return string
   *   The user account name.
   */
  public function alterUserTitle(?UserInterface $user = NULL): string {
    return $user ? $user->getDisplayName() : '';
  }

}
