<?php

namespace Drupal\steam_login\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Steam OpenId' Block.
 *
 * @Block(
 *   id = "steam_openid",
 *   admin_label = @Translation("Steam OpenId"),
 * )
 */
class SteamOpenIdLogInOut extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The steam_login module path.
   *
   * @var string
   */
  protected $modulePath;

  /**
   * Steam OpenId block constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected ConfigFactoryInterface $configFactory,
    protected ModuleHandler $moduleHandler,
    protected AccountProxyInterface $account,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->modulePath = $moduleHandler->getModule('steam_login')->getPath();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $img1 = $this->buildImg(1);
    $img2 = $this->buildImg(2);

    $form['steam_login_button'] = [
      '#type' => 'radios',
      '#options' => [
        0 => $this->renderer->render($img1),
        1 => $this->renderer->render($img2),
      ],
      '#default_value' => $this->configFactory->getEditable('steam_login.config')
        ->get('login_button'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configFactory->getEditable('steam_login.config')
      ->set('login_button', $form_state->getValue('steam_login_button'))
      ->save();

    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];

    if (!$this->account->isAuthenticated()) {
      switch ($this->configFactory->getEditable('steam_login.config')->get('login_button')) {
        case 0:
          $img = $this->buildImg(1);
          break;

        case 1:
          $img = $this->buildImg(2);
          break;

        default:
          return $build;
      }

      $build['steam_openid'] = [
        '#type' => 'link',
        '#title' => $img,
        '#url' => Url::fromRoute('steam_login.openid'),
      ];

      return $build;
    }

    $build['account'] = [
      '#type' => 'link',
      '#title' => $this->account->getDisplayName(),
      '#url' => Url::fromRoute('entity.user.canonical', ['user' => $this->account->id()]),
    ];
    $build['separator'] = [
      '#markup' => ' | ',
    ];
    $build['logout'] = [
      '#type' => 'link',
      '#title' => 'Logout',
      '#url' => Url::fromRoute('user.logout'),
    ];

    return $build;
  }

  /**
   * Build steam image.
   *
   * @param int $id
   *   Either 1 or 2.
   *
   * @return array
   *   A renderable array representing the image.
   */
  protected function buildImg(int $id): array {
    switch ($id) {
      case 1:
        return [
          '#theme' => 'image',
          '#uri' => '/' . $this->modulePath . '/images/openid_01.png',
          '#attributes' => [
            'alt' => $this->t("Sign in through Steam"),
          ],
          '#width' => 180,
          '#height' => 35,
        ];

      case 2:
        return [
          '#theme' => 'image',
          '#uri' => '/' . $this->modulePath . '/images/openid_02.png',
          '#attributes' => [
            'alt' => $this->t("Sign in through Steam"),
          ],
          '#width' => 109,
          '#height' => 66,
        ];

      default:
        return [];
    }
  }

}
