<?php

namespace Drupal\steam_login\Hook;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Alter displayed user entity.
 */
class UserAlter implements ContainerInjectionInterface {

  /**
   * User storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * Class constructor.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Alter User Name.
   *
   * @param string $name
   *   The user name being altered.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   */
  #[Hook('user_format_name_alter')]
  public function alterUserName(string &$name, AccountInterface $account): void {
    if (str_starts_with($name, 'steam-')) {
      /** @var ?\Drupal\user\Entity\User $user */
      $user = $this->userStorage->load($account->id());
      if ($user) {
        $steamUsername = current($user->get('field_steam_username')->getValue());
        $name = isset($steamUsername['value']) ? urldecode($steamUsername['value']) : $name;
      }
    }
  }

}
