<?php

namespace Drupal\steam_login\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Provides help for modules users.
 */
class Help {

  use StringTranslationTrait;

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function __invoke(string $route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      case 'help.page.steam_login':
        // Introduction.
        $help = '<h2>' . $this->t('Introduction') . '</h2>';
        $help .= '<p>' . $this->t('<strong>Steam Login</strong> allows users to connect to your website using their Steam account.') . '</p>';
        $help .= '<p>' . $this->t('The module provides a <i>Steam OpenId</i> block displaying a configurable image button (both images are official) leading anonymous users to the Steam authentication system secured with Steam guard.') . '<br/>';
        $help .= $this->t('It also provides a callback page checking that the user is correctly logged in through Steam before to create / update his account on the website.') . '<br/>';
        $help .= $this->t('As soon as Steam Login is enabled, 2 fields are added to the user profile:') . '</p>';
        $help .= '<ul>';
        $help .= '<li>' . $this->t('field_steam64id stores the Steam community ID') . '</li>';
        $help .= '<li>' . $this->t('field_steam_username stores the Steam username') . '</li>';
        $help .= '</ul>';
        $help .= '<p>' . $this->t('For users having a field_steam64id value, displayed username on the website is set to that Steam username.') . '</p>';
        // Configuration.
        $help .= '<h2>' . $this->t('Configuration') . '</h2>';
        $help .= '<p>' . $this->t('After the module has been enabled, you still need to:') . '</p>';
        $help .= '<ul>';
        $help .= '<li>' . $this->t('<a href=":cfg_api_key">Configure your steam API key</a>', [
          ':cfg_api_key' => Url::fromRoute('steam_api.settings')->toString(),
        ]) . '</li>';
        $help .= '<li>' . $this->t('Go to <a href=":cfg_accounts">/admin/config/people/accounts</a>, in the section "Who can register accounts?" choose "Visitors" and uncheck "Require email verification when a visitor creates an account" (we need visitors to create their account via steam connect, and we don\'t know their email)', [
          ':cfg_accounts' => Url::fromRoute('entity.user.admin_form')->toString(),
        ]) . '</li>';
        $help .= '<li>' . $this->t('Add the provided Steam OpenId block to a region using the block manager <a href=":cfg_blocks">/admin/structure/block</a>.', [
          ':cfg_blocks' => Url::fromRoute('block.admin_display')->toString(),
        ]) . '</li>';
        $help .= '</ul>';
        return $help;
    }
  }

}
