# STEAM PROFILE

## Introduction
**Steam Profile** provides a steam community block for users logged in via their
Steam guard.

## Configuration
- Add the provided "User Steam community" block to a region using the block
manager `/admin/structure/block`. Set the routes restriction of this block to
`entity.user.canonical`.
