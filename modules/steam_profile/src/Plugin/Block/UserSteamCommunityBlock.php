<?php

namespace Drupal\steam_profile\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\steam_profile\Service\SteamCommunityUserParserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'User Steam community' Block.
 *
 * @Block(
 *   id = "steamcommunity_user",
 *   admin_label = @Translation("User Steam community"),
 * )
 */
class UserSteamCommunityBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Block constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\steam_profile\Service\SteamCommunityUserParserInterface $steamUserParser
   *   Steam community user parser.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    protected RequestStack $requestStack,
    protected SteamCommunityUserParserInterface $steamUserParser,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('steam_profile.parse_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    if ($viewed_user = $this->requestStack->getCurrentRequest()->get('user')) {
      if ($viewed_user->hasField('field_steam64id')) {
        $steam_community_id = $viewed_user->get('field_steam64id')->value;
        if (!empty($steam_community_id)) {
          $data = $this->steamUserParser->getUserInfos($steam_community_id);
          $build = [
            '#theme' => 'steamcommunity_user',
            '#data' => Json::decode($data),
            '#attached' => [
              'library' => [
                'steam_profile/steamcommunity_user',
              ],
              'drupalSettings' => [
                'steam_profile' => [
                  'community_id' => $steam_community_id,
                ],
              ],
            ],
            '#cache' => [
              'contexts' => [
                'user',
              ],
              'max-age' => 0,
            ],
          ];
        }
      }
    }

    return $build;
  }

}
