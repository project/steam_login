<?php

namespace Drupal\steam_profile\Hook;

use Drupal\Core\Hook\Attribute\Hook;

/**
 * Theme hooks implementations.
 */
class Theme {

  /**
   * Implements hook_theme().
   */
  #[Hook('theme')]
  public function __invoke(): array {
    return [
      'steamcommunity_user' => [
        'template'  => 'steamcommunity-user',
        'variables' => [
          'data'      => NULL,
        ],
      ],
    ];
  }

}
