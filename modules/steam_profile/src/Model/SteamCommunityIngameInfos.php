<?php

namespace Drupal\steam_profile\Model;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Set steam community user ingame infos from parsed data.
 */
class SteamCommunityIngameInfos {

  /**
   * The game name.
   *
   * @var string
   */
  public $gameName;

  /**
   * The game icon.
   *
   * @var string
   */
  public $gameIcon;

  /**
   * The game join link.
   *
   * @var \Drupal\Core\Link|\Drupal\Core\GeneratedLink
   */
  public $gameJoinLink;

  /**
   * Set the ingame infos properties from parsed data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   *
   * @return self
   *   The current object.
   */
  public function buildIngameInfos($data): self {
    $this->setGameName($data);
    $this->setGameIcon($data);
    $this->setGameJoinLink($data);

    return $this;
  }

  /**
   * Set the game name from parsed data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setGameName($data): void {
    if (!empty((string) $data->inGameInfo->gameName)) {
      $this->gameName = (string) $data->inGameInfo->gameName;
    }
  }

  /**
   * Set the game icon from parsed data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setGameIcon($data): void {
    if (!empty($data->inGameInfo->gameIcon)) {
      $this->gameIcon = (string) $data->inGameInfo->gameIcon;
    }
  }

  /**
   * Set the game join link from parsed data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setGameJoinLink($data): void {
    if (!empty($data->inGameInfo->gameJoinLink)) {
      $url = Url::fromUri((string) $data->inGameInfo->gameJoinLink);
      $this->gameJoinLink = Link::fromTextAndUrl('Join', $url)->toString();
    }
  }

}
