<?php

namespace Drupal\steam_profile\Model;

use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Build a steam community profile.
 */
class SteamCommunityUser {

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\steam_profile\Model\SteamCommunityIngameInfos $steamCommunityIngameInfos
   *   The steam community ingame infos builder.
   */
  public function __construct(
    protected DateFormatterInterface $dateFormatter,
    protected SteamCommunityIngameInfos $steamCommunityIngameInfos,
  ) {
  }

  /**
   * The account privacy state.
   *
   * @var bool
   */
  public $privacyState;

  /**
   * The user steam id 64.
   *
   * @var string
   */
  public $steamId64;

  /**
   * The user steam URL.
   *
   * @var string
   */
  public $steamUrl;

  /**
   * The user name.
   *
   * @var string
   */
  public $name;

  /**
   * The account VAC banned state.
   *
   * @var bool
   */
  public $vacBanned;

  /**
   * The user avatar icon URL.
   *
   * @var string
   */
  public $avatarIcon;

  /**
   * The user medium icon URL.
   *
   * @var string
   */
  public $avatarMedium;

  /**
   * The user avatar full URL.
   *
   * @var string
   */
  public $avatarFull;

  /**
   * The user online state.
   *
   * @var string
   */
  public $onlineState;

  /**
   * The user steam rating.
   *
   * @var int
   */
  public $steamRating;

  /**
   * The user real name.
   *
   * @var string
   */
  public $realname;

  /**
   * The user location.
   *
   * @var string
   */
  public $location;

  /**
   * The user ingame data.
   *
   * @var \Drupal\steam_profile\Model\SteamCommunityIngameInfos
   */
  public $inGameInfo;

  /**
   * The user steam account creation date.
   *
   * @var string
   */
  public $memberSince;

  /**
   * Build user data from parsed data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   *
   * @return self
   *   The current object.
   */
  public function buildUserInfos($data): self {
    $this->buildUserInfosPublic($data);

    if ($data->privacyState) {
      $this->buildUserInfosProtected($data);
    }

    return $this;
  }

  /**
   * Build user public data from parsed data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function buildUserInfosPublic($data): void {
    $this->setPrivacyState($data);

    if (!empty($data->steamID64)) {
      $this->setSteamId64($data);
      $this->setSteamUrl($data);
    }
    $this->setSteamName($data);
    $this->setVacBanned($data);
    $this->setAvatarIcon($data);
    $this->setAvatarMedium($data);
    $this->setAvatarFull($data);
  }

  /**
   * Set user's protected data.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function buildUserInfosProtected($data) {
    $this->setOnlineState($data);
    $this->setIngameInfo($data);
    $this->setSteamRating($data);
    $this->setMemberSince($data);
    $this->setRealName($data);
    $this->setLocation($data);
  }

  /**
   * Set the user privacy state.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setPrivacyState($data): void {
    if (!empty($data->privacyState)) {
      $this->privacyState = ('public' == (string) $data->privacyState);
    }
  }

  /**
   * Set the user steam ID 64.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setSteamId64($data): void {
    $this->steamId64 = (string) $data->steamID64;
  }

  /**
   * Set the user steam community url.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setSteamUrl($data): void {
    if (!empty($data->customURL)) {
      $this->steamUrl = "https://steamcommunity.com/id/{$data->customURL}";
    }
    else {
      $this->steamUrl = "https://steamcommunity.com/profiles/{$data->steamID64}";
    }
  }

  /**
   * Set the user steam name.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setSteamName($data): void {
    if (!empty($data->steamID)) {
      $this->name = (string) htmlspecialchars_decode($data->steamID);
    }
  }

  /**
   * Set the user vac banned status.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setVacBanned($data): void {
    if (!empty($data->vacBanned)) {
      $this->vacBanned = (bool) $data->vacBanned;
    }
  }

  /**
   * Set the user icon avatar.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setAvatarIcon($data): void {
    if (!empty($data->avatarIcon)) {
      $this->avatarIcon = (string) $data->avatarIcon;
    }
  }

  /**
   * Set the user medium avatar.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setAvatarMedium($data): void {
    if (!empty($data->avatarMedium)) {
      $this->avatarMedium = (string) $data->avatarMedium;
    }
  }

  /**
   * Set the user full avatar.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setAvatarFull($data): void {
    if (!empty($data->avatarFull)) {
      $this->avatarFull = (string) $data->avatarFull;
    }
  }

  /**
   * Set the user online state.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setOnlineState($data): void {
    if (!empty($data->onlineState)) {
      $this->onlineState = (string) $data->onlineState;
    }
  }

  /**
   * Set the user steam rating.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setSteamRating($data): void {
    if (!empty($data->steamRating)) {
      $this->steamRating = (int) $data->steamRating;
    }
  }

  /**
   * Set the user real name.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setRealName($data): void {
    if (!empty($data->realname)) {
      $this->realname = (string) $data->realname;
    }
  }

  /**
   * Set the user location.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setLocation($data): void {
    if (!empty($data->location)) {
      $this->location = (string) $data->location;
    }
  }

  /**
   * Set the user ingame infos.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setIngameInfo($data): void {
    if (!empty($data->inGameInfo)) {
      $this->inGameInfo = $this->steamCommunityIngameInfos->buildIngameInfos($data);
    }
  }

  /**
   * Set the user membership date.
   *
   * @param object $data
   *   Data from steam community xml feed.
   */
  protected function setMemberSince($data): void {
    if (!empty($data->memberSince)) {
      $this->memberSince = ucfirst(
        $this->dateFormatter->format(strtotime((string) $data->memberSince), 'medium_date')
      );
    }
  }

}
