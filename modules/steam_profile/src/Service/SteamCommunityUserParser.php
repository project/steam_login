<?php

namespace Drupal\steam_profile\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\steam_profile\Model\SteamCommunityUser;
use GuzzleHttp\Client;

/**
 * Parse steam community object.
 */
class SteamCommunityUserParser implements SteamCommunityUserParserInterface {

  use StringTranslationTrait;

  /**
   * Service constructor.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   A guzzle http client instance.
   * @param \Drupal\steam_profile\Model\SteamCommunityUser $steamCommunityUser
   *   The steam community user builder.
   */
  public function __construct(
    protected Client $httpClient,
    protected SteamCommunityUser $steamCommunityUser,
  ) {
  }

  /**
   * Retrieve user data from Steam Community through its id.
   *
   * @param string $steamCommunityId
   *   The user steam community id.
   */
  public function getUserInfos($steamCommunityId) {
    // Default response.
    $response = [
      'error' => $this->t('Steam data currently not available for this user.'),
    ];

    $xml_url = 'https://steamcommunity.com/profiles/' . $steamCommunityId . '/?xml=1&' . time();

    try {
      $request = $this->httpClient->get($xml_url, [
        'headers' => [
          'Accept' => 'text/xml',
        ],
      ]);
      $data = (string) $request->getBody();
      $data = simplexml_load_string($data);
      if (isset($data->error)) {
        $response = [
          'error' => (string) $data->error,
        ];
      }
      else {
        $response = $this->steamCommunityUser->buildUserInfos($data);
      }
    }
    catch (\Exception $e) {
      $response = [
        'error' => $e->getMessage(),
      ];
    }

    return Json::encode($response);
  }

}
