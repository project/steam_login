<?php

namespace Drupal\steam_profile\Service;

/**
 * Interface for steam community user parser objects.
 */
interface SteamCommunityUserParserInterface {

  /**
   * Retrieve user data from Steam Community through its id.
   *
   * @param string $steam_community_id
   *   The user steam community id.
   */
  public function getUserInfos($steam_community_id);

}
