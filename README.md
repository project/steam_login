# STEAM LOGIN

## Introduction
**Steam Login** allows users to connect to your website using their Steam
account.

The module provides a *Steam OpenId* block displaying a configurable image
button (both images are official) leading anonymous users to the Steam
authentication system secured with Steam guard.
It also provides a callback page checking that the user is correctly logged in
through Steam before to create / update his account on the website.
As soon as **Steam Login** is enabled, 2 fields are added to the user profile:
- `field_steam64id` stores the Steam community ID
- `field_steam_username` stores the Steam username

For users having a field_steam64id value, displayed username on the website is
set to that Steam username.

## Configuration
After the module has been enabled, you still need to:

- [Configure your steam API key](https://www.drupal.org/project/steam_api#desc-config)
- Go to `/admin/config/people/accounts`, in the section "Who can register
accounts?" choose "Visitors" and uncheck "Require email verification when a
visitor creates an account" (we need visitors to create their account via steam
connect, and we don't know their email)
- Add the provided Steam OpenId block to a region using the block manager
`/admin/structure/block`.

## Submodules
- [Steam Profile](modules/steam_profile/README.md)
